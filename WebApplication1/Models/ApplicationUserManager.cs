﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ApplicationUserManager2 : UserManager<ApplicationUser>
    {
        public ApplicationUserManager2(IUserStore<ApplicationUser> store) : base(store)
        {
        }

        public static ApplicationUserManager2 Create(
            IdentityFactoryOptions<ApplicationUserManager2> oprions, 
            IOwinContext context)
        {
            ApplicationDbContext db = context.Get<ApplicationDbContext>();
            ApplicationUserManager2 manager = new ApplicationUserManager2(
                new UserStore<ApplicationUser>(db));
            return manager;
        }
    }
}