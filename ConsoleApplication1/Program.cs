﻿using KatanaWebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Microsoft.Owin.Hosting.WebApp.Start<Startup1>("http://localhost:8000"))
            {
                Console.WriteLine("Server is loading. For exit press any key");
                Console.ReadLine();
            }
        }
    }
}
